package net.ukr.wumf;

import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Введите слово англ. буквами, превратим в ASCII-art: ");
		String text = scan.nextLine();
		scan.close();
		StrToAsciiConv tr = new StrToAsciiConv();

		List<List<String>> arrStr = tr.StrToAsciiConvert(text);
		tr.asciiOut(arrStr);

	}

}
